# This upgrade file is only for the k8s version 1.18.
# For now we keep for every minor update a separate upgrade file
# to avoid accidental major changes of old upgrade files.

- name: Detect user
  hosts: k8s_nodes
  gather_facts: false
  # we need *all* hosts.
  any_errors_fatal: true
  vars_files:
    - ../vars/etc.yaml
  roles:
  - detect_user

- name: Run pre-flight checks on all nodes
  hosts: k8s_nodes
  gather_facts: true
  roles:
    - kubernetes-upgrade-info
  tags:
    - kubernetes-upgrade-preflight-checks

- name: Upgrade the first master node
  hosts: masters[0]
  gather_facts: false
  any_errors_fatal: true
  vars_files:
    - ../vars/disruption.yaml
  tags:
    - kubeadm-first-master
  pre_tasks:
    - name: include ksl vars
      when: ksl_vars_directory is defined
      include_vars:
        dir: "{{ ksl_vars_directory }}"
      tags:
        - always
  roles:
    - name: cluster-health-verification
      delegate_to: localhost
      when: not (k8s_skip_upgrade_checks | bool)
    - name: kubeadm-drain-node
      when: "not (k8s_upgrade_done | default(False))"
    - name: update_system
      when: "not (k8s_upgrade_done | default(False))"
    - name: kubeadm-upgrade-kubeadm
      when: "not (k8s_upgrade_done | default(False))"
    - name: kubeadm-upgrade-apply
      when: "not (k8s_upgrade_done | default(False))"
    - name: kubeadm-uncordon-node
      when: "not (k8s_upgrade_done | default(False))"

- name: Upgrade the residual master nodes
  hosts: masters[1:]
  gather_facts: false
  serial: 1
  any_errors_fatal: true
  vars_files:
    - ../vars/disruption.yaml
  tags:
    - kubeadm-other-masters
  pre_tasks:
    - name: include ksl vars
      when: ksl_vars_directory is defined
      include_vars:
        dir: "{{ ksl_vars_directory }}"
      tags:
        - always
  roles:
    - name: cluster-health-verification
      delegate_to: localhost
      when: not (k8s_skip_upgrade_checks | bool)
    - name: kubeadm-drain-node
      when: "not (k8s_upgrade_done | default(False))"
    - name: update_system
      when: "not (k8s_upgrade_done | default(False))"
    - name: kubeadm-upgrade-kubeadm
      when: "not (k8s_upgrade_done | default(False))"
    - name: kubeadm-upgrade-node
      when: "not (k8s_upgrade_done | default(False))"
    - name: kubeadm-uncordon-node
      when: "not (k8s_upgrade_done | default(False))"

- name: Upgrade kubelet on master nodes
  hosts: masters
  gather_facts: false
  any_errors_fatal: true
  tags:
    - kubelet-masters
  serial: 1
  roles:
    - name: kubeadm-upgrade-kubelet
      when: "not (k8s_upgrade_done | default(False))"
      # we do not want to customize kubelet on control-plane nodes
      vars:
        k8s_kubelet_disable_customizations: true

- name: Upgrade the worker nodes
  hosts: workers
  gather_facts: false
  any_errors_fatal: true
  vars_files:
    - ../vars/disruption.yaml
  tags:
    - workers
  serial: 1
  pre_tasks:
    - name: include ksl vars
      when: ksl_vars_directory is defined
      include_vars:
        dir: "{{ ksl_vars_directory }}"
      tags:
        - always
  roles:
    - name: cluster-health-verification
      delegate_to: localhost
      when: not (k8s_skip_upgrade_checks | bool)
    - name: kubeadm-drain-node
      when: "not (k8s_upgrade_done | default(False))"
    - name: update_system
      when: "not (k8s_upgrade_done | default(False))"
    - name: kubeadm-upgrade-kubeadm
      when: "not (k8s_upgrade_done | default(False))"
    - name: kubeadm-upgrade-node
      when: "not (k8s_upgrade_done | default(False))"
    - name: kubeadm-upgrade-kubelet
      when: "not (k8s_upgrade_done | default(False))"
    - name: kubeadm-uncordon-node
      when: "not (k8s_upgrade_done | default(False))"

- name: Final upgrade note
  gather_facts: false
  hosts: masters[0]
  tasks:
  - name: Final upgrade note
    debug:
      msg: |
        🥳 Congratulations! 🎉

          \o/  The upgrade to
           |      {{ next_k8s_version }}
          / \  is complete.

        You can now change the k8s_version variable to
        {{ next_k8s_version | to_json }}.
