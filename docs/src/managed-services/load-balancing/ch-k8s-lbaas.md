# ch-k8s-lbaas

The ch-k8s-lbaas load-balancing solution resides in its own [official repository](https://github.com/cloudandheat/ch-k8s-lbaas).

## Functionality Visualization

<img src="./../../img/ch-k8s-lbaas-overview.svg" alt="ch-k8s-lbaas Overview Visualization" width="100%">

## ch-k8-lbaas Configuraton

```toml
{{#include ./../../templates/config.template.toml:ch-k8s-lbaas_config}}
```
