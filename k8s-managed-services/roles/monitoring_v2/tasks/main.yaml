---
- name: "{{ monitoring_internet_probe | ternary('I', 'Uni') }}nstall blackbox-exporter"
  community.kubernetes.helm:
    chart_ref: prometheus-blackbox-exporter
    chart_repo_url: https://prometheus-community.github.io/helm-charts
    chart_version: 5.3.1
    release_namespace: "{{ monitoring_namespace }}"
    release_name: "{{ kms_blackbox_exporter_release_name }}"
    release_state: "{{ monitoring_internet_probe | ternary('present', 'absent') }}"
    values: "{{ lookup('template', 'blackbox_exporter.yaml.j2') | from_yaml }}"
    wait: yes
    update_repo_cache: yes

- name: Expose Prometheus with a NodePort
  when: k8s_monitoring_enabled and k8s_global_monitoring_enabled
  tags:
    - ch-k8s-global-monitoring
    - ch-k8s-global-monitoring-nodeport
  k8s:
    apply: yes
    definition: "{{ lookup('template', 'prometheus-nodeport.yaml') }}"
    validate:
      fail_on_error: yes
      strict: yes

- name: Create architecture-specific service monitors
  block:
  - name: Create keepalived service monitor (frontend)
    k8s:
      state: present
      apply: yes
      definition: "{{ lookup('template', 'keepalived-service-monitor.yaml.j2') }}"
      validate:
        fail_on_error: yes
        strict: yes

  - name: Create HAProxy service monitor (frontend)
    k8s:
      state: present
      apply: yes
      definition: "{{ lookup('template', 'haproxy-service-monitor.yaml.j2') }}"
      validate:
        fail_on_error: yes
        strict: yes

  - name: Create ch-k8s-LBaaS service monitors (frontend)
    k8s:
      definition: "{{ lookup('template', 'lbaas-service-monitor.yaml.j2') }}"
      apply: yes
      validate:
        fail_on_error: yes
        strict: yes
    when: ch_k8s_lbaas_enabled

  - name: Create rook-ceph-mgr service monitor
    k8s:
      state: present
      apply: yes
      definition: "{{ lookup('template', 'rook-service-monitor.yaml.j2') }}"
      validate:
        fail_on_error: yes
        strict: yes
    when: k8s_storage_rook_enabled

  - name: Create service monitor node-exporters (frontend)
    k8s:
      state: present
      apply: yes
      definition: "{{ lookup('template', 'node-exporter-service-monitor.yaml') }}"
      validate:
        fail_on_error: yes
        strict: yes
    tags:
      - node-exporter-service-monitor

- name: "{{ k8s_is_gpu_cluster | ternary('I', 'Uni') }}nstall NVIDIA DCGM exporter"
  community.kubernetes.helm:
    chart_ref: dcgm-exporter
    chart_repo_url: https://nvidia.github.io/dcgm-exporter/helm-charts
    chart_version: 2.6.10
    release_namespace: "{{ monitoring_namespace }}"
    release_name: "nvidia-dcgm-exporter"
    release_state: "{{ k8s_is_gpu_cluster | ternary('present', 'absent') }}"
    values: "{{ lookup('template', 'nvidia_dcgm_exporter.yaml.j2') | from_yaml }}"
    wait: yes
    update_repo_cache: yes
